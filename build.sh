


   
#!/bin/bash
set -xe

[ -d build ] || git clone https://gitlab.com/Vin4ter/halium-generic-adaptation-build-tools -b main build
./build/build.sh "$@"
